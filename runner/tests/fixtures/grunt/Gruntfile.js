/* eslint-env node */
module.exports = function ( grunt ) {
	var conf = grunt.file.readJSON( 'extension.json' );

	grunt.loadNpmTasks( 'grunt-jsonlint' );
	grunt.loadNpmTasks( 'grunt-eslint' );
	grunt.loadNpmTasks( 'grunt-banana-checker' );
	grunt.loadNpmTasks( 'grunt-stylelint' );

	grunt.initConfig( {
		banana: {
			all: conf.MessagesDirs,
			dev: conf.MessagesDirs.TestFooBar,
			options: {
				disallowDuplicateTranslations: false,
				disallowUnusedTranslations: false,
				requireLowerCase: 'initial',
			},
		},
		eslint: {
			all: '.',
			options: {
				cache: true,
				reportUnusedDisableDirectives: true,
				fix: grunt.option( 'fix' )
			}
		},
		jsonlint: {
			all: [
				'**/*.json',
				'!node_modules/**',
				'!vendor/**'
			]
		},
		stylelint: {
			all: [
				'**/*.css',
				// Some comment
				'!node_modules/**',
				'!vendor/**'
			]
		}
	} );

	grunt.registerTask( 'test', [ 'jsonlint', 'banana', 'eslint', 'stylelint' ] );
};
