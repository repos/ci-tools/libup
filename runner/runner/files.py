"""
Copyright (C) 2019 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from __future__ import annotations

import glob
import json
from collections import OrderedDict, defaultdict
from collections.abc import Iterable
from os import PathLike
from typing import Optional

import braceexpand

NODE_MODULES_FOLDER_PREFIX = "node_modules/"


def load_ordered_json(fname: PathLike | str) -> OrderedDict:
    with open(fname) as f:
        return json.load(f, object_pairs_hook=OrderedDict)


def save_pretty_json(data: dict, fname: PathLike | str):
    with open(fname, "w") as f:
        out = json.dumps(data, indent="\t", ensure_ascii=False)
        f.write(out + "\n")


def glob_patterns(original: Iterable[str]) -> set[str]:
    patterns: set[str] = set()
    for pattern in original:
        patterns.update(braceexpand.braceexpand(pattern))
    return patterns


class PackageJson:
    # TODO: Support non-dev deps
    def __init__(self, fname) -> None:
        self.fname = fname
        self.data = load_ordered_json(self.fname)

    def get_packages(self):
        return list(self.data.get("devDependencies", {}))

    def has_package(self, package: str) -> bool:
        return package in self.data["devDependencies"]

    def get_version(self, package: str) -> Optional[str]:
        try:
            return self.data["devDependencies"][package]
        except KeyError:
            return None

    def set_version(self, package: str, version: str):
        if package in self.data.get("devDependencies", {}):
            self.data["devDependencies"][package] = version
            return

        raise RuntimeError(f"Unable to set version for {package} to {version}")

    def remove_package(self, package):
        if "devDependencies" in self.data:
            del self.data["devDependencies"][package]
            return

        raise RuntimeError(f"Unable to remove {package}")

    def save(self):
        save_pretty_json(self.data, self.fname)


class PackageLockJson:
    def __init__(self, fname="package-lock.json"):
        self.fname = fname
        self.data = load_ordered_json(self.fname)
        if self.data["lockfileVersion"] == 1:
            raise RuntimeError("lockfileVersion 1 is no longer supported")
        if self.data["lockfileVersion"] > 3:
            raise RuntimeError("lockfileVersion > 3 is not supported")

        self.workspaces = set()
        for pattern in glob_patterns(self.data["packages"][""].get("workspaces", [])):
            # TODO: this doesn't really work if fname is not the default. In Python 3.11
            # glob() can take root_dir to fix that.
            self.workspaces.update(glob.glob(pattern))

        self.versions = defaultdict(set)
        for package, pdata in self.data["packages"].items():
            if package == "":
                continue
            if "resolved" in pdata and pdata["resolved"] in self.workspaces:
                continue

            if "name" in pdata:
                # Included NPM workspaces will have the name in the lock file directly.
                package_name = pdata["name"]
            else:
                if pdata.get("link", False):
                    # If this is a link to a subpath of the local repository
                    # there will be no useful information in this lockfile.
                    # We could read the subpath but we don't have a use for that
                    # information so don't bother.
                    continue
                if not package.startswith(NODE_MODULES_FOLDER_PREFIX):
                    # Again, subpath packages could be looked up with some effort
                    # but we don't need to do that as we have no use for that.
                    continue

                # Otherwise, guess the name from the folder name.
                try:
                    index = package.rindex(NODE_MODULES_FOLDER_PREFIX)
                except ValueError as e:
                    if package in self.workspaces:
                        # Workspaces where the package is marked as private
                        # will not have any name information. We don't really
                        # care about versions of those, so just skip it.
                        continue

                    raise ValueError(
                        f"Failed to extract package name from '{package}'"
                    ) from e
                package_name = package[index + len(NODE_MODULES_FOLDER_PREFIX) :]

            self.versions[package_name].add(pdata.get("version", "(unknown)"))

    def get_version(self, package: str) -> set[str]:
        return self.versions[package].copy()

    def set_version(self, package: str, version: str):
        raise NotImplementedError

    def save(self):
        # For rollbacks and stuff. We don't allow modification though.
        save_pretty_json(self.data, self.fname)


class ComposerJson:
    # TODO: Support non-dev deps
    def __init__(self, fname):
        self.fname = fname
        self.data = load_ordered_json(self.fname)

    def get_version(self, package: str) -> Optional[str]:
        if package in self.data.get("require-dev", {}):
            return self.data["require-dev"][package]
        if "extra" in self.data:
            suffix = package.split("/")[-1]
            if suffix in self.data["extra"]:
                return self.data["extra"][suffix]

        return None

    def get_extra(self, extra):
        if "extra" in self.data:
            if extra in self.data["extra"]:
                return self.data["extra"][extra]

        return None

    def set_version(self, package, version):
        if package in self.data.get("require-dev", {}):
            self.data["require-dev"][package] = version
            return
        if "extra" in self.data:
            suffix = package.split("/")[-1]
            if suffix in self.data["extra"]:
                self.data["extra"][suffix] = version
                return

        raise RuntimeError(f"Unable to set version for {package} to {version}")

    def add_package(self, package: str, version: str):
        if "require-dev" not in self.data:
            self.data["require-dev"] = {}

        self.data["require-dev"][package] = version

    def remove_package(self, package):
        if package in self.data.get("require-dev", {}):
            del self.data["require-dev"][package]
            return

        raise RuntimeError(f"Unable to remove {package}")

    def remove_extra(self, extra):
        if "extra" in self.data:
            if extra in self.data["extra"]:
                del self.data["extra"][extra]
                if len(self.data["extra"]) == 0:
                    del self.data["extra"]
                return

        raise RuntimeError(f"Unable to remove {extra}")

    def save(self):
        if "require-dev" in self.data:
            # Re-sort dependencies by package name
            self.data["require-dev"] = OrderedDict(
                sorted(self.data["require-dev"].items(), key=lambda x: x[0])
            )
        save_pretty_json(self.data, self.fname)
