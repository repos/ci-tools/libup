"""
Copyright (C) 2024 Taavi Väänänen <taavi@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from collections import defaultdict
from typing import Iterable

from flask_sqlalchemy import SQLAlchemy
from prometheus_client import Metric
from prometheus_client.metrics_core import CounterMetricFamily
from prometheus_client.registry import REGISTRY, Collector
from sqlalchemy import func

from libup.model import Log, Repository


class LibupCollector(Collector):
    def __init__(self, db: SQLAlchemy) -> None:
        self.db = db

    def collect(self) -> Iterable[Metric]:
        yield CounterMetricFamily(
            "libup_runs",
            "LibUp finished runs",
            value=self.db.session.query(func.max(Log.id)).scalar(),
        )

        repos_total: dict[str, int] = defaultdict(lambda: 0)
        repos_by_type: dict[str, dict[str, int]] = defaultdict(
            lambda: defaultdict(lambda: 0)
        )
        repos_failing_total: dict[str, int] = defaultdict(lambda: 0)
        repos_failing_by_type: dict[str, dict[str, int]] = defaultdict(
            lambda: defaultdict(lambda: 0)
        )

        for repo in self.db.session.query(Repository).all():
            types = [
                key
                for key, value in {
                    "canary": repo.is_canary,
                    "wikimedia": repo.is_wm_deployed,
                    "bundled": repo.is_bundled,
                }.items()
                if value
            ]

            repos_total[repo.branch] += 1
            for repo_type in types:
                repos_by_type[repo.branch][repo_type] += 1

            if repo.is_error:
                repos_failing_total[repo.branch] += 1
                for repo_type in types:
                    repos_failing_by_type[repo.branch][repo_type] += 1

        m_repos_total = CounterMetricFamily(
            "libup_repos_total",
            "LibUp total repository count",
            labels=["branch"],
        )
        for branch, count in repos_total.items():
            m_repos_total.add_sample(
                m_repos_total.name, {"branch": branch}, value=count
            )
        yield m_repos_total
        m_repos_by_type = CounterMetricFamily(
            "libup_repos_by_type",
            "LibUp repository count by type",
            labels=["type"],
        )
        for branch, by_type in repos_by_type.items():
            for repo_type, count in by_type.items():
                m_repos_by_type.add_sample(
                    m_repos_by_type.name,
                    {"branch": branch, "type": repo_type},
                    value=count,
                )
        yield m_repos_by_type

        m_errors_total = CounterMetricFamily(
            "libup_errors_total",
            "LibUp total failing repository count",
            labels=["branch"],
        )
        for branch, count in repos_failing_total.items():
            m_errors_total.add_sample(
                m_errors_total.name, {"branch": branch}, value=count
            )
        yield m_errors_total
        m_errors_by_type = CounterMetricFamily(
            "libup_errors_by_type",
            "LibUp failing repository count by type",
            labels=["type"],
        )
        for branch, by_type in repos_failing_by_type.items():
            for repo_type, count in by_type.items():
                m_errors_by_type.add_sample(
                    m_errors_by_type.name,
                    {"branch": branch, "type": repo_type},
                    value=count,
                )
        yield m_errors_by_type


def setup_metrics(db: SQLAlchemy):
    REGISTRY.register(LibupCollector(db))
