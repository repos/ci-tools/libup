FROM rust:bookworm AS rust-builder
RUN cargo install gerrit-grr
RUN cargo install cargo-audit
RUN cargo install package-lock-lint --git https://gitlab.com/taavivaananen/package-lock-lint.git --rev 1a99e3a877d2113e636b3727caefc05eb370f7e7

FROM docker-registry.wikimedia.org/bookworm:latest
ENV LANG C.UTF-8
ENV CHROME_BIN /usr/bin/chromium
ENV CHROMIUM_FLAGS "--no-sandbox"
RUN apt-get update && \
    apt-get install -y firefox-esr chromium
RUN apt-get install -y nodejs npm git openssh-client curl unzip \
    ruby ruby-dev rubygems-integration \
    build-essential pkg-config \
    php-ast php-cli php-xml php-zip php-gd php-excimer \
    php-gmp php-mbstring php-curl php-intl php-yaml \
    php-igbinary php-xdebug php-ldap php-redis php-pcov \
    composer \
    python3 python3-dev python3-venv \
    # explicitly include libssl, for grr
    libssl3 \
    # for mathoid
    librsvg2-dev \
    --no-install-recommends && rm -rf /var/lib/apt/lists/* && \
    # xdebug slows everything down, it'll be manually enabled as needed
    phpdismod xdebug
RUN gem install jsduck
COPY --from=rust-builder /usr/local/cargo/bin/grr /usr/bin/grr
COPY --from=rust-builder /usr/local/cargo/bin/package-lock-lint /usr/bin/package-lock-lint
COPY --from=rust-builder /usr/local/cargo/bin/cargo-audit /usr/bin/cargo-audit
COPY files/gitconfig /etc/gitconfig
COPY files/timeout-wrapper.sh /usr/local/bin/timeout-wrapper

RUN install --owner=nobody --group=nogroup --directory /cache
RUN install --owner=nobody --group=nogroup --directory /src
RUN install --owner=nobody --group=nogroup --directory /venv
# Some tooling (e.g. git config) is easier if we have a home dir.
RUN install --owner=nobody --group=nogroup --directory /nonexistent

USER nobody
ENV PYTHONUNBUFFERED 1
RUN python3 -m venv /venv/ && /venv/bin/pip install -U wheel
COPY runner/setup.py /src/
COPY runner/requirements.txt /src/
RUN cd src/ \
    && /venv/bin/pip install -r requirements.txt
COPY ./runner/runner /src/runner
RUN cd src/ \
    && /venv/bin/python3 setup.py install
ENV COMPOSER_PROCESS_TIMEOUT 1800
# Shared cache
ENV NPM_CONFIG_CACHE=/cache
ENV XDG_CACHE_HOME=/cache
ENV BROWSERSLIST_IGNORE_OLD_DATA="yes"
WORKDIR /src
ENTRYPOINT ["/usr/local/bin/timeout-wrapper"]
CMD ["runner"]
